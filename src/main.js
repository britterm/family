import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import MainPage from './components/MainPage'

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '*',
    component: MainPage,
    props: (route) => ({
      headerText: route.query.text
    })
  }]
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
